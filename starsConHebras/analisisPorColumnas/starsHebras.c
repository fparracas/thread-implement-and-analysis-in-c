/******************************************************************************
 * 
 * FILE: par-stars-250914.c 
 *
 * DESCRIPTION: Determina la posición de estrellas en un sector del cielo
                tomando como entrada un número entero para la creación de 
                una matriz, para el análisis se omitió la lectura de los datos
                del archivo stars-16.txt, y se modificó el código para que 
                solo se creará la matriz, para así realizar un óptimo análisis.
 *      
 * 
 * Autor: Felipe Parra Castañeda
 *
 * Última revisión: Santiago de Chile, 26/11/2018
 *
 *****************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define STAR    '*'
#define NOSTAR  ' '
#define SILENT  0
#define VERBOSE 1


struct Message {
   int myid, rvalue, cvalue, size, opmode;
};

unsigned char **sky;
char **map;

/*
 * 
 * 
 */
void *Process(void *p) {
  
   int i, j, k;
   float sum, v;
   struct Message *m;
  
   m = (struct Message *) p;
   if (m->opmode == VERBOSE) {
      printf("\n\n**************************************\n\n");
      printf("From %d Rows = %d Cols = %d Size = %d - Beginning The Task\n\n",m->myid,m->rvalue,m->cvalue,m->size);
   }     
   for (i = 1; i <= m->rvalue; i = i + 1)
      for (j = m->cvalue, k = 1; k <= m->size; j = j + 1, k = k + 1) {
         sum  = (float)(sky[j][i] + sky[j - 1][i] + sky[j][i + 1] + sky[j + 1][i] + sky[j][i - 1]);
         v = sum / 5.0;
   if (v > 6.0)
      map[j-1][i-1] = STAR;
         else
      map[j-1][i-1] = NOSTAR;
      } 
   if (m->opmode == VERBOSE)
      printf("From %d Ending The Task\n\n",m->myid);   
   pthread_exit(0);
}   


/*
 * 
 * 
 */
void PrintMap(int r, int c) {
  
   int i, j;
 
   printf("\n+++++++++++++++ MAP +++++++++++\n\n");
   for (i = 0; i <= r; i = i + 1)
      printf("-");
   printf("\n");
    for (i = 0; i < r; i = i + 1) {
       printf("|");
       for (j = 0; j < c; j = j + 1)
    printf("%c",map[j][i]);
       printf("\n");
    }
}


/*
 * 
 * 
 */
void PrintData(int r, int c) {
  
   int i, j;
 
   printf("\n+++++++++++++++ DATA +++++++++++\n\n");
    for (i = 1; i <= r; i = i + 1) {
       for (j = 1; j <= c; j = j + 1)
    printf(" %2d ",sky[j][i]);
       printf("\n");
    }
}


/*
 * 
 * 
 */
void ReadData(int r, int c) {

   int i, j; 
   sky = calloc(r + 2,sizeof(unsigned char *));
   for (i = 0; i < r + 2; i = i + 1)
      sky[i] = calloc(c + 2,sizeof(unsigned char));
}


/*
 * 
 * 
 */
void Usage(char *message) {
  
   printf("\nUsage: %s -S K N",message);
   printf("\n\n\n\n");
   printf("K: Cantidad de hebras en que se dividirán los procesos\n\n");
   printf("N: Tamaño de la matriz\n\n");

}   
   
/*
 * 
 * 
 */
int main(int argc, char **argv) {

   int i, k, r, c, mode, s, rem, l;
   pthread_t *thread;
   pthread_attr_t attribute;
   struct Message **m;
   void *exit_status;   

   if (argc == 4) {
      

      if (strcmp(argv[1],"-S") == 0)
      
   mode = SILENT;
      if (strcmp(argv[1],"-V") == 0)
         mode = VERBOSE;
      k = atoi(argv[2]);

      //Se define el tamaño de la matriz mediante el segundo argumento enviado por la terminal:
      r = c = atoi(argv[3]);
      //Se utiliza ReadData para la creación de la matriz.
      ReadData(r,c);

      if (mode == 1)
      PrintData(r,c);
      map = calloc(r,sizeof(char *));
      for (i = 0; i < r; i = i + 1)
         map[i] = calloc(c,sizeof(char));      
      thread = calloc(k,sizeof(pthread_t));
      m = calloc(k,sizeof(struct Message *));
      for (i = 0; i < k; i = i + 1)
         m[i] = calloc(1,sizeof(struct Message));
      pthread_attr_init(&attribute);
      pthread_attr_setdetachstate(&attribute,PTHREAD_CREATE_JOINABLE);
      s = c / k;
      rem = c % k;
      l = 1;
      time_t  t0, t1; /* time_t is defined on <time.h> and <sys/types.h> as long */
      clock_t c0, c1; /* clock_t is defined on <time.h> and <sys/types.h> as int */

      t0 = time(NULL);
      c0 = clock();

      for (i = 0; i < k; i = i + 1) {
            if (mode == VERBOSE) 
                printf("Main: creating thread %d\n", i);
            m[i]->myid = i;
            m[i]->rvalue = r;
           if (rem != 0) {
               m[i]->size = s + 1;
                rem = rem - 1;
             }
           else
              m[i]->size = s;
          m[i]->cvalue = l;
          m[i]->opmode = mode;
          l = l + m[i]->size;
         pthread_create(&thread[i],&attribute,Process,(void *) m[i]);
      }      
       t1 = time(NULL);
      c1 = clock();


      printf("Hebras: %d\n", k);
      printf("Tamaño matriz: %dx%d\n",r,r);
      printf ("lapsed CPU time: %f\n\n", (float) (c1 - c0)/CLOCKS_PER_SEC);

      pthread_attr_destroy(&attribute); 

      for (i = 0; i < k; i = i + 1)
         pthread_join(thread[i],&exit_status);
      //PrintMap(r,c);      

     

      
  }
   else
      Usage(argv[0]);
   return 0;
}   