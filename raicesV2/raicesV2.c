/*
/******************************************************************************
 * 
 * FILE:  raicesV2.c
          Se obtuvo tras modificar el archivo: par-squares-g-v2.c 
 *
 * DESCRIPTION: Calcula las raíces de 1 hasta n números, donde el n se ingresa a través
                de la terminal, sin utilizar variable global.
                Se quitaron todos los printf, dejando solamente aquellos que muestran el
                tiempo que tardó en realizar la operación

 *      
 * 
 * Autor: Felipe Parra Castañeda
 *
 *****************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


struct Message {

   int myid, nvalue, numthreads;
};

float *result;   

/*
 *
 */
void *Computes(void *n) {

   int i, j, size;
   struct Message *m;
   m = (struct Message *) n;
   size = m->nvalue / m->numthreads;
   for (i = 0, j = m->myid * size + 1; i < size; i = i + 1, j = j + 1)
      result[j-1] = sqrt(j);
   pthread_exit(0);
}


/*
 *
 */
void *Usage(char *argv[]) {

   printf("Usage: %s n k\n", argv[0]);
   exit(1);
}


/*
 *
 */
int main(int argc, char *argv[]) {

   time_t t0, t1;
   clock_t c0, c1;

   pthread_t *thread;
   pthread_attr_t attribute;
   struct Message **m;
   void *exit_status;
   int n, i, k;

   if (argc != 3)
      Usage(argv);
   else {
      n = atoi(argv[1]);
      k = atoi(argv[2]);
      thread = calloc(k,sizeof(pthread_t));
      result = calloc(n,sizeof (float *));
      m = calloc(k,sizeof(struct Message *));
      for (i = 0; i < k; i = i + 1)
         m[i] = calloc(1,sizeof(struct Message));
      pthread_attr_init(&attribute);
      pthread_attr_setdetachstate(&attribute,PTHREAD_CREATE_JOINABLE);
      
      t0 = time(NULL);
      c0 = clock();

      //Operaciones principales
      //Creación de hebras.
      for (i = 0; i < k; i = i + 1) {
         m[i]->myid = i;
         m[i]->nvalue = n;
         m[i]->numthreads = k;
         pthread_create(&thread[i],&attribute,Computes,(void *) m[i]);
      }

      pthread_attr_destroy(&attribute); 
      for (i = 0; i < k; i = i + 1)
         pthread_join(thread[i],&exit_status);

   }


      // Se declaran las últimas variables de tiempo
      t1 = time(NULL);
      c1 = clock();
      // Fun medición de tiempo


      printf("\nNum entrada: %d\nThreads: %d\n", n, k);
      printf ("lapsed CPU time: %f\n", (float) (c1 - c0)/CLOCKS_PER_SEC);
   
   return 0;
}
