/******************************************************************************
 * 
 * FILE:  raicesV3.c
          Se obtuvo tras modificar el archivo: par-squares-v3.c 
 *
 * DESCRIPTION: Calcula las raíces de 1 hasta n números, donde el n se ingresa a través
                de la terminal, sin utilizar variable global.
                Se quitaron todos los printf, dejando solamente aquellos que muestran el
                tiempo que tardó en realizar la operación

 *      
 * 
 * Autor: Felipe Parra Castañeda
 *
 *****************************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


struct Message {

   int myid, nvalue, numthreads;
   float *data;
};   

/*
 *
 */
void *Computes(void *n) {

   int i, j, size;
   struct Message *m;
   float *s;

   fflush(stdout);
   m = (struct Message *) n;
   fflush(stdout);
   size = m->nvalue / m->numthreads;
   s = m->data;
   for (i = 0, j = m->myid * size + 1; i < size; i = i + 1, j = j + 1) {
      s[i] = sqrt(j);
      fflush(stdout);
   } 
   fflush(stdout);
   for (i = 0; i < size; i++) {
      fflush(stdout);
   }   
   pthread_exit((void *) m);
}

void *Usage(char *argv[]) {

   printf("Usage: %s n k\n", argv[0]);
   exit(1);
}


int main(int argc, char *argv[]) {

   time_t  t0, t1;
   clock_t c0, c1;

   pthread_t *thread;
   pthread_attr_t attribute;
   struct Message **m;
   void *exit_status;
   int n, ii, j, k, l, rc;
   float **s;

   if (argc != 3)
      Usage(argv);
   else {
      n = atoi(argv[1]);
      k = atoi(argv[2]);
      thread = calloc(k,sizeof(pthread_t));
      s = calloc(k,sizeof(float *));
      m = calloc(k,sizeof(struct Message *));
      for (ii = 0; ii < k; ii = ii + 1)
         s[ii] = calloc(n / k,sizeof(float));
      for (ii = 0; ii < k; ii = ii + 1)
         m[ii] = calloc(1,sizeof(struct Message));
      pthread_attr_init(&attribute);
      pthread_attr_setdetachstate(&attribute,PTHREAD_CREATE_JOINABLE);


      t0 = time(NULL);
      c0 = clock();

      for (ii = 0; ii < k; ii = ii + 1) {
         //printf("Main: creating thread %d\n", ii);
         m[ii]->myid = ii;
         m[ii]->nvalue = n;
         m[ii]->numthreads = k;
         m[ii]->data = s[ii];
         rc = pthread_create(&thread[ii],&attribute,Computes,(void *) m[ii]);
         //printf("**** %d.- rc = %d\n",ii,rc);
      }


      pthread_attr_destroy(&attribute); 
      for (ii = 0; ii < k; ii = ii + 1) {
         rc = pthread_join(thread[ii],&exit_status);
         //printf("i = %d rc = %d \n",ii,rc);
         m[ii] = (struct Message *) exit_status;
         //printf("%d.- Message Received From %d\n",ii+1,m[ii]->myid);
         s[ii] = m[ii]->data;
         //sleep(3);
      }

      t1 = time(NULL);
      c1 = clock();
      printf("\nNum entrada: %s\n Threads: %s\n", argv[1], argv[2]);
      printf ("\nElapsed CPU time:        %f\n\n", (float) (c1 - c0)/CLOCKS_PER_SEC);



      //printf("\n\n**************************************\n\n");
      /*for (j = 0, l = 1; j < k; j = j + 1)
         for (ii = 0; ii < n / k; ii = ii + 1, l = l + 1) 
            printf("%3d - %f\n",l,s[j][ii]);
      */
   }
   return 0;
}
