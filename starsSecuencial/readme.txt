Para la compilación y ejecución de los códigos, realizar los siguientes pasos:

Secuencual:

gcc -pthread -o starsSecuencial starsSecuencial.c

Luego para utilizar el programa:

./starsSecuencial -S n


donde -S se mantiene para todos los casos, ya que no se utilizó la opción -V para las pruebas.
n = tamaño de la matriz que se desea analizar.