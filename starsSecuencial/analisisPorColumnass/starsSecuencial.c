/******************************************************************************
 * 
 * FILE: starsSecuencial.c 
 *
 * DESCRIPCIÓN: Modificación del archivo stars-2014.c, el cual leía los datos de un archivo
                que representaban la intensidad de luz para ubicar estrellas, se le hicieron
                modificaciones para que solo reciba un n de entrada, y genere la matriz de
                tamaño nxn, para realizar un análisis del rendimiendo del programa con distintos
                n de entradas.
                En este se realizará el análisis, recorriendo la matriz por columnas y luego por filas.


 * 
 * AUTOR: Feipe Parra Castañeda
 *
 * LAST REVISED: Santiago de Chile, 30/9/2014
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <string.h>

#define STAR    '*'
#define NOSTAR  ' '
#define SILENT  0
#define VERBOSE 1


/*
 * 
 * 
 */
char **Process(unsigned char **sky, int r, int c) {
  
   char **map;
   int i, j;
   float sum, v;
   double time_spent;
   time_t t0, t1;
   clock_t c0, c1;


   t0 = time(NULL);
   c0 = clock();
   
   map = calloc(r,sizeof(char *));
   for (i = 0; i < r; i = i + 1)
      map[i] = calloc(c,sizeof(char));


   for (i = 1; i <= r; i = i + 1)
      for (j = 1; j <= c; j = j + 1) {
         sum  = (float)(sky[j][i] + sky[j - 1][i] + sky[j][i + 1] + sky[j + 1][i] + sky[j][i - 1]);
         v = sum / 5.0;
	 if (v > 6.0)
	    map[j-1][i-1] = STAR;
         else
	    map[j-1][i-1] = NOSTAR;
      }

  t1 = time(NULL);
  c1 = clock();

  printf("Numero entrada: %d\n", c);
  printf("elapsed CPU time:%f\n\n", (float) (c1 - c0)/CLOCKS_PER_SEC);
  return map;
}   


/*
 * 
 * 
 */
void PrintMap(char **map, int r, int c) {
  
   int i, j;
 
   printf("\n+++++++++++++++ MAP +++++++++++\n\n");
   for (i = 0; i <= r; i = i + 1)
      printf("-");
   printf("\n");
    for (i = 0; i < r; i = i + 1) {
       printf("|");
       for (j = 0; j < c; j = j + 1)
	  printf("%c",map[j][i]);
       printf("\n");
    }
}


/*
 * 
 * 
 */
void PrintData(unsigned char **sky, int r, int c) {
  
   int i, j;
 
   printf("\n+++++++++++++++ DATA +++++++++++\n\n");
    for (i = 1; i <= r; i = i + 1) {
       for (j = 1; j <= c; j = j + 1)
	  printf(" %2d ",sky[j][i]);
       printf("\n");
    }
}


unsigned char **ReadData(int r, int c) {

   int i, j;
   unsigned char **sky;
   
   sky = calloc(r + 2,sizeof(unsigned char *));
   for (i = 0; i < r + 2; i = i + 1)
      sky[i] = calloc(c + 2,sizeof(unsigned char));
   return sky;
}


void Usage(char *message) {
  
   printf("\nUsage: %s -S n",message);
   printf("\n\nn = tamaño de matriz\n\n");
}   
   
int main(int argc, char **argv) {

   int r, c, mode;
   unsigned char **sky;
   char **map;

   if (argc == 3) {
      if (strcmp(argv[1],"-S") == 0)
	 mode = SILENT;
      if (strcmp(argv[1],"-V") == 0)
         mode = VERBOSE;   
      //scanf("%d",&r);
      //scanf("%d",&c);

      r = c = atoi(argv[2]);
      sky = ReadData(r,c);
      if (mode == VERBOSE){

      }
	 /*PrintData(sky,r,c);*/
      else
        map = Process(sky,r,c);
      /*PrintMap(map,r,c); */     
  }
   else
      Usage(argv[0]);
}   
